local selection = {}
selection.image = love.graphics.newImage('select.png')
selection.current = nil

function selection.update()
    love.graphics.setCanvas(tempLayers.selection)
    love.graphics.clear()
    love.graphics.setCanvas(tempLayers.selectionRect)
    love.graphics.clear()
    if selection.current then
        local sel = selection.current
        love.graphics.setColor(0, 0, 0)
        drawables.dashRect(sel.x, sel.y, sel.w, sel.h, 6, 4)
        if sel.canvas then
            love.graphics.setCanvas(tempLayers.selection)
            love.graphics.draw(sel.canvas, sel.x, sel.y)
            love.graphics.setCanvas(activeLayer)
        end
    end
    love.graphics.setCanvas(activeLayer)
end

function selection.mousepressed(x, y, button)
    if x > 0 and x < attributes.width
    and y > 0 and y < attributes.height then
        if not selection.current then
            -- Create new selection
            tempLayers.selectionRect = love.graphics.newCanvas(attributes.width, attributes.height)
            selection.current = {}
            selection.current.ox = x
            selection.current.oy = y
            selection.current.x = x
            selection.current.y = y
            selection.current.w = 0
            selection.current.h = 0
            selection.current.mode = 'create'
        elseif x > selection.current.x and x < selection.current.x + selection.current.w
        and y > selection.current.y and y < selection.current.y + selection.current.h then
            selection.current.mode = 'move'
        else
            love.graphics.setCanvas(activeLayer)
            love.graphics.draw(selection.current.canvas, selection.current.x, selection.current.y)
            selection.current = nil
        end
    end
end

function selection.mousereleased(button, x, y)
    local sel = selection.current
    if sel then
        if sel.w == 0 or sel.h == 0 then
            sel = nil
        else
            if sel.mode == 'create' then
                tempLayers.selection = love.graphics.newCanvas(attributes.width, attributes.height)
                sel.canvas = love.graphics.newCanvas(sel.w, sel.h)
                local quad = love.graphics.newQuad(sel.x, sel.y, sel.w, sel.h, attributes.width, attributes.height)
                love.graphics.setCanvas(sel.canvas)
                love.graphics.draw(activeLayer, quad)
                love.graphics.setCanvas(activeLayer)
                love.graphics.setBlendMode('replace')
                love.graphics.setColor(0, 0, 0, 0)
                    love.graphics.rectangle('fill', sel.x, sel.y, sel.w, sel.h)
                love.graphics.setColor(255, 255, 255, 255)
                love.graphics.setBlendMode('alpha')
            end
            sel.mode = 'done'
        end
    end
end

function selection.mousemoved(x, y, dx, dy)
    if selection.current then
        if selection.current.mode == 'create' then
            selection.current.x = math.min(x, selection.current.ox)
            selection.current.y = math.min(y, selection.current.oy)
            selection.current.w = math.abs(x - selection.current.ox)
            selection.current.h = math.abs(y - selection.current.oy)
        elseif selection.current.mode == 'move' then
            selection.current.x = selection.current.x + dx
            selection.current.y = selection.current.y + dy
        end
    end
end

return selection
