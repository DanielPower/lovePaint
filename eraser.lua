local eraser = {}
eraser.image = love.graphics.newImage('eraser.png')

function eraser.mousepressed(button, x, y)
    love.graphics.setBlendMode('subtract')
    love.graphics.setBlendMode('alpha')
end

function eraser.mousereleased(button, x, y)
end

function eraser.mousemoved(x, y, dx, dy)
    if love.mouse.isDown(1) then
        love.graphics.setBlendMode('replace')
        love.graphics.setColor(0, 0, 0, 0)
            love.graphics.line(x-dx, y-dy, x, y)
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setBlendMode('alpha')
    end
end

return eraser
