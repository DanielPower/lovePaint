local pencil = {}
pencil.image = love.graphics.newImage('pencil.png')

function pencil.mousepressed(button, x, y)
end

function pencil.mousereleased(button, x, y)
end

function pencil.mousemoved(x, y, dx, dy)
    if love.mouse.isDown(1) then
        love.graphics.setColor(primaryColor)
        love.graphics.line(x-dx, y-dy, x, y)
        love.graphics.setColor(blank)
    end
end

return pencil
