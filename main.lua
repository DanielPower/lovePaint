drawables = require('drawables')

function love.load()
    ui = {
        sidebar = require('sidebar'),
    }

    tools = {
        require('pencil'),
        require('eraser'),
        require('selection'),
    }

    attributes = {
        width = 800,
        height = 600,
    }

    -- Draw transparent background
    background = love.graphics.newCanvas(attributes.width, attributes.height)
    love.graphics.setCanvas(background)
    drawables.checkers(attributes.width, attributes.height, 16)
    love.graphics.setCanvas()

    layers = {
        love.graphics.newCanvas(attributes.width, attributes.height)
    }

    tempLayers = {}

    activeLayer = layers[1]
    activeTool = tools[1]
end

function love.update(dt)
    love.graphics.setCanvas(activeLayer)
    if activeTool.update then
        activeTool.update(dt)
    end
    love.graphics.setCanvas()
end

function love.draw()
    love.graphics.clear(64, 69, 82)

    -- Interface
    ui.sidebar.draw()

    love.graphics.setCanvas()
    -- Draw layers to canvas
    love.graphics.draw(background, ui.sidebar.width)

    for _, layer in ipairs(layers) do
        love.graphics.draw(layer, ui.sidebar.width)
    end

    for _, layer in pairs(tempLayers) do
        love.graphics.draw(layer, ui.sidebar.width)
    end
end

function love.mousepressed(x, y, button, istouch)
    if button == 3 then
        love.load()
    end
    ui.sidebar.mousepressed(x, y, button, istouch)

    local x = x - ui.sidebar.width
    love.graphics.setCanvas(activeLayer)
    if activeTool.mousepressed then
        activeTool.mousepressed(x, y, button, istouch)
    end
    love.graphics.setCanvas()
end

function love.mousereleased(x, y, button, istouch)
    local x = x - ui.sidebar.width
    love.graphics.setCanvas(activeLayer)
    if activeTool.mousereleased then
        activeTool.mousereleased(x, y, dx, dy, istouch)
    end
    love.graphics.setCanvas()
end

function love.mousemoved(x, y, dx, dy, istouch)
    local x = x - ui.sidebar.width
    love.graphics.setCanvas(activeLayer)
    if activeTool.mousemoved then
        activeTool.mousemoved(x, y, dx, dy, istouch)
    end
    love.graphics.setCanvas()
end
