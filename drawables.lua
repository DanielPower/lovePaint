local drawables = {}

function drawables.checkers(width, height, tileSize)
    local tileX = math.ceil(width/tileSize)
    local tileY = math.ceil(height/tileSize)

    local light = {255, 255, 255}
    local dark = {200, 200, 200}
    local color
    local offset = 0

    for x=1, tileX do
        for y=1, tileY do
            if ((y+offset)/2 == math.ceil((y+offset)/2)) then
                color = light
            else
                color = dark
            end

            love.graphics.setColor(color)
            love.graphics.rectangle('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
        end
        offset = offset + 1
    end
end

function drawables.dashRect(x, y, width, height, dashLength, gapLength)
    love.graphics.setLineWidth(1)
    love.graphics.setLineStyle('rough')

    local i = 0
    while i < width do
        love.graphics.line(x+i, y, math.min(x+i+dashLength, x+width), y)
        love.graphics.line(x+width-i, y+height, math.max(x+width-i-dashLength, x), y+height)
        i = i + dashLength + gapLength
    end

    local i = 0
    while i < height do
        love.graphics.line(x+width, y+i, x+width, math.min(y+i+dashLength, y+height))
        love.graphics.line(x, y+height-i, x, (math.max(y+height-i-dashLength, y)))
        i = i + dashLength + gapLength
    end
    love.graphics.setLineWidth(lineWidth)
    love.graphics.setLineStyle(lineStyle)
end

function drawables.dashPoly(polygon)
end

function drawables._dashSwitch(dash, remaining, dashLength, gapLength)
    remaining = remaining - 1
    if remaining == 0 then
        dash = not dash
        if dash then
            remaining = dashLength
        else
            remaining = gapLength
        end
    end

    return dash, remaining
end

return drawables
