local sidebar = {}
sidebar.width = 32

function sidebar.draw()
    -- Style
    love.graphics.setLineStyle('rough')
    love.graphics.setLineWidth(1)

    -- Background
    love.graphics.setColor(53, 57, 69)
    love.graphics.rectangle('fill', 0, 0, sidebar.width, love.graphics.getHeight())
    love.graphics.setColor(43, 46, 57)

    for i, tool in ipairs(tools) do
        local x = 4
        local y = 4 + (i-1)*32

        if tool == activeTool then
            love.graphics.setColor(255, 255, 255)
        else
            love.graphics.setColor(43, 46, 57)
        end
        love.graphics.rectangle('line', x, y, 24, 24)
        love.graphics.setColor(255, 255, 255)
        love.graphics.draw(tool.image, x, y)
    end

    -- Unstyle
    love.graphics.setLineStyle(lineStyle)
    love.graphics.setLineWidth(lineWidth)
end

function sidebar.mousepressed(x, y)
    if x > 0 and x < sidebar.width then
        local button = math.ceil(y/32)
        if tools[button] then
            activeTool = tools[button]
        end
    end
end

return sidebar
